#!/bin/sh

err(){
echo "Usage:
	optimize -p input -o output
Options:
	-p: input PDF
	-o: output PDF
If no output or output file is specified you will be prompted to enter one." && exit 1 ;
}

while getopts "p:o:" o; do case "${o}" in
	p) file="${OPTARG}" ;;
	o) output="${OPTARG}" ;;
	*) printf "Invalid option" && err ;;
esac done

shift $((OPTIND - 1))

[ -z "$file" ] && echo "Please enter a name of the file you want to optimize: " && read -r output

[ ! -f "$file" ] && echo "Please check the PDF you entered exists" && err

[ -z "$output" ] && echo "Please enter a name for the new PDF: " && read -r output

ps2pdf -dOptimize=true -dCompatibilty=1.3 -dPDFSETTINGS=/ebook $file $output && echo "Done"
