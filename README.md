# PDF Optimizer

A simple bash script to optimize PDF files. The resulting PDF should be compatible with and display much faster on most kindle models.

## Usage
    -p: input pdf
    -a: output pdf
If no files are specified you will be prompted for them.


## Requirements
    - ps2pdf
Most distros ship the utility as part of the post script package.
